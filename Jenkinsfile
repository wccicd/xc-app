/* groovylint-disable LineLength */


pipeline {
    agent  any

    environment {
      DOCKER_REG=""
      RELEASE_VERSION="9.1.5.0"
      BUILD_IMAGE="ts-utils"
      BUILD_LABEL="${CUSTCODE}-${IMAGE_TYPE}-${BUILD_NUMBER}"
      COMMENT="# DO NOT ADD A / AT THE END"
      DOCKER_REG_PATH="gcr.io/project_name/commerce"
      BUILD_TYPE = 'xc'
      BUNDLE_DIR = '../../bundle-zips'
      CUSTCODE = 'cus'
      IMAGE_REPO='commerce-product/release/9.1'
      DOSFTP = 'false'
      DROP_LOC = '/tmp'
      GIT_DOTAGS = 'true'
      GITOPS_PUSH = 'true'
      USER_EMAIL = 'jenkins@test.com'
      IMAGE_TYPE = 'xc-app'
      SLACK_URL="false"
      WCBD_BUNDLE_NAME = 'wcbd_files.zip'
      DOCKER_KEY= '/root/gcp-service.key'
      GIT_HELM_REPO_BASE_DIR = 'helmcharts'
      GIT_HELM_REPO_URL = 'gitlab.com/uri/helmchart.git'
      AUTH_DIR = 'auth'
      LIVE_DIR = 'live'
    }

    stages {

      stage ('Starting runSQL job') {
        steps{
          sh '''
          echo "running.....Not really"
          '''
        //        // This stage calls jobs for dataload, runSQL and acpload with parameters before the xc-app build starts.
        //        stage ('Starting runSQL job') {
        //          steps {
        //         build job: 'acpload',  parameters: [
        //        [$class: 'StringParameterValue', name: 'Environment', value: String.valueOf(params.Environment)]
        //         ], propagate:false
        //        build job: 'dataload', parameters: [
        //        [$class: 'StringParameterValue', name: 'Environment', value: String.valueOf(params.Environment)]
        //       ], propagate: false
        //        build job: 'runsql', parameters: [
        //        [$class: 'StringParameterValue', name: 'Environment', value: String.valueOf(params.Environment)]
        //        ],propagate: false

        }
      }

      stage("Refresh Util image"){
        steps {
          sh '''
            chmod 600 $DOCKER_KEY
            cat $DOCKER_KEY | docker login -u _json_key --password-stdin https://${DOCKER_REG_PATH}

            #! /bin/bash
            set -e

            #exec 1>log
            #exec 2>err

            #set +e
            ## stop and remove utility docker
            echo "log] Removing current image..."
            echo "log] ${BUILD_IMAGE}"
            echo
            docker stop ${BUILD_IMAGE} || true
            docker rm ${BUILD_IMAGE} || true
            # #docker rmi "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_IMAGE}:${BUILD_IMAGE_TAG}" || true


            # set -x
            set -e

            ## Set the image name to be used in pull and starting of container
            echo "log] Setting DOCKER_FULL_BUILD_IMAGE image variable..."
            echo
            export DOCKER_FULL_BUILD_IMAGE="${DOCKER_REG_PATH}/${BUILD_IMAGE}:${RELEASE_VERSION}"


            ## Pull the build image
            echo "log] Pulling image..."
            echo "log] ${DOCKER_FULL_BUILD_IMAGE}"
            echo
            docker pull "${DOCKER_FULL_BUILD_IMAGE}"

            ERROR_CODE=$?


            if [ "$ERROR_CODE" -ne "0" ]; then
              echo
              echo "ERR] Error while pulling build docker image.  Exiting."
              echo
              exit -1
            fi

            # Start build utility container:
            echo "log] Starting image..."
            echo "log] ${BUILD_IMAGE}"
            echo
            docker run -d -e LICENSE=accept --name ${BUILD_IMAGE} "${DOCKER_FULL_BUILD_IMAGE}"

          '''
        }
      }

      stage('WCB: Prepping the environment') {
          steps{
                sh '''

                  # Create directory for source in BUILD_IMAGE docker:
                  docker exec ${BUILD_IMAGE} bash -c "mkdir -p /opt/WebSphere/CommerceServer90/src/${BUILD_TYPE}"

                  # Copy custom code(the git repo of BUILD_TYPE) for image we are building into the build container:
                  docker cp . ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/src/${BUILD_TYPE}
                  chmod 755 build.sh
                  docker cp build.sh ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/
                  docker cp wcbd-build-shared-classpath.xml ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/
                  docker cp wcbd-build-xc-definition.properties ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/
                  docker cp build.properties ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/
                  docker cp build.private.properties ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/
                  '''
          }
      }
      stage('WCB: Doing the *BuildBundle* ') {
          steps{
                sh '''

                  # Execute build of customization package inside of the build container:
                  docker exec ${BUILD_IMAGE} bash -c "cd /opt/WebSphere/CommerceServer90/wcbd && ./build.sh ${CUSTCODE} ${BUILD_TYPE} ${BUILD_NUMBER} ${DROP_LOC}"

                  # Copy the bundle zip from the build container back into the jenkins workspace:
                  bundle_fname=wcbd-deploy-server-${CUSTCODE}-${BUILD_TYPE}-${BUILD_NUMBER}.zip
                  docker cp ${BUILD_IMAGE}:/tmp/$bundle_fname docker-build/${BUILD_TYPE}

                  # Copy the logs to log dir
                  mkdir -p /var/jenkins_home/workspace/xc-app/logs
                  docker cp ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/logs /var/jenkins_home/workspace/xc-app/logs
                '''
          }
      }

      stage ('Copying *BuildBundle* outside of ts-util ') {
        steps{
              sh '''
                # Copy the bundle zip from the build container back into the jenkins workspace:
                bundle_fname=wcbd-deploy-server-${CUSTCODE}-${BUILD_TYPE}-${BUILD_NUMBER}.zip
                docker cp ${BUILD_IMAGE}:/tmp/$bundle_fname docker-build/${BUILD_TYPE}

                # Copy the logs to log dir
                mkdir -p /var/jenkins_home/workspace/xc-app/logs
                docker cp ${BUILD_IMAGE}:/opt/WebSphere/CommerceServer90/wcbd/logs /var/jenkins_home/workspace/xc-app/logs
              '''
        }
      }


      stage('Docker Build') {
        steps {
          sh '''
            ##########################################################################################
            # Step 0 - Pull the OOTB Image
            ##########################################################################################
            # Pull the OOTB Docker image
            ## Set the image name to be pull
            echo "log] Setting DOCKER_OOTB_FULL_IMAGE_TOBUILD image variable..."
            echo
            export DOCKER_OOTB_FULL_IMAGE_TOBUILD="${DOCKER_REG_PATH}/${IMAGE_TYPE}:${RELEASE_VERSION}"

            echo "log] Pulling image..."
            echo "log] DOCKER_OOTB_FULL_IMAGE_TOBUILD=${DOCKER_OOTB_FULL_IMAGE_TOBUILD}"
            echo

            set +e
            docker pull "${DOCKER_OOTB_FULL_IMAGE_TOBUILD}"
            ERROR_CODE=$?
            set -e

            ## Check if the image exists
          #  if [ "$ERROR_CODE" -eq "0" ]; then
          #    echo "========================================================="
          #    echo " Image already exists ${DOCKER_REG}/${DOCKER_NAMESPACE}/${IMAGE_TYPE}:${BUILD_LABEL}"
          #    echo "========================================================="
          #    echo
          #    exit -1;
          #  fi

          '''

          sh '''
           ##########################################################################################
           # Step 1 - Unzip file - Cleanup any possible directory, then recreate it
           ##########################################################################################

           # [deprecated] call and execute the docker build shell script
           # [replacement] this is now done within the jenkins file
           # cd docker-build
           # ./docker-build.sh $BUILD_TYPE $BUILD_NUMBER $IMAGE_TYPE $CUSTCODE  --build-arg image_repo=$image_repo --build-arg image_tag=$BUILD_IMAGE_TAG
           # Setting the current directory
           cd docker-build/$BUILD_TYPE

           echo "log] Current Directory"
           echo "current dir=`pwd`"
           echo "log]"

           rm -fr CusDeploy
           ls -lrt

           mkdir CusDeploy
           unzip -q -d CusDeploy *${BUILD_NUMBER}.zip

          '''

          sh '''
           ##########################################################################################
           # Step 2 - Docker Build
           ##########################################################################################

           # Setting the current directory
           cd docker-build/$BUILD_TYPE

           #BUILD_LABEL=${CUSTCODE}-${IMAGE_TYPE}-${BUILD_NUMBER}
           echo "log] BUILD_LABEL=${BUILD_LABEL}"

           ls -lrt
           more Dockerfile


           docker build -t "${DOCKER_REG_PATH}/${IMAGE_TYPE}:${BUILD_LABEL}" --build-arg IMAGE_REPO="${DOCKER_REG_PATH}" --build-arg IMAGE_TAG="${RELEASE_VERSION}" .

          '''

          sh '''
           ##########################################################################################
           # Step 4 - Docker Push
           ##########################################################################################


           echo "Pushing ${DOCKER_REG_PATH}/${IMAGE_TYPE}:${BUILD_LABEL}"

           docker push "${DOCKER_REG_PATH}/${IMAGE_TYPE}:${BUILD_LABEL}"

          '''

          sh '''
           ##########################################################################################
           # Step 4 - Clean Up
           #
           ##########################################################################################
           # Setting the current directo
           cd docker-build/$BUILD_TYPE

           rm -rf CusDeploy
           rm -fr *${BUILD_LABEL}.zip
           rm Dockerfile
          '''

        }

      }

      stage('Helm Update and GIT Push') {

        steps {

          git branch: 'master', credentialsId: 'test-jenkins', url: 'https://gitlab.com/saurabhumathe/helmchart.git'
          sh '

            # update the values file in helm chart with the build number
            ls -lrt
            cd hcl-commerce-helmchart/stable/hcl-commerce

            # Update values.yaml to have new build number
            fname=values.yaml
            grep -i 'image:'  ${fname} | grep "${IMAGE_TYPE}"
            currenttag=`awk 'c&&!--c;/name: '${IMAGE_TYPE}'/{c=3}' ${fname} | awk '{print $2}'`
            sed '/name: xc-app/ { n; n; n;  s/tag: '${currenttag}'/tag: '${RELEASE_VERSION}'-'${BUILD_NUMBER}'/; }' ${fname} > updatedValues.yaml
            mv updatedValues.yaml ${fname}
            cat ${fname}
            git add ${fname}
            ls -lrt
            pwd
            git config credential.https://gitlab.com.username jenkins-user
            git config --global user.email "wcbbuilder@hcl.com"
            git config --global credential.helper "!echo password=${GIT_PASS}; echo"
            git commit -m "updated values.yaml to use BUILD NUMBER: ${BUILD_NUMBER} build tag" ./${fname}
            git push --set-upstream origin master
          '''
        }
      }
    }
}

post {
  success {
    sh '''
      #stop and remove utility docker
      # docker stop ${BUILD_IMAGE}
      # docker rm ${BUILD_IMAGE}
      #docker rmi "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_IMAGE}:${BUILD_IMAGE_TAG}"
    '''
  }
  failure {

    sh '''
      # if slack is present
      #if [[ "${SLACK_URL}" != "false" ]]
      #then
      #   MESSAGE="BUILD FAILED! Please go here ${RUN_DISPLAY_URL}"
      #   curl -X POST -H 'Content-type: application/json' --data "{'text':\\"${MESSAGE}\\"}" ${SLACK_URL}
      #fi
    '''
  }
}
