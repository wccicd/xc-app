# xc-app

Extended Customizations repo

# Description
This is a sample file on how to build your jenkins pipeline and git repository to adopt a CICD process. You will need to modify it, adapt it and own it as part of your project. The modifications required are particular to your environment like container registry, keys for access to the container registry, check in procedure, HCK Commerce version to be used, build tag customization and others.

# Skills required
- DevOps
- HCL Commerce

# How it works
The Jenkinsfiles is where you want to start to read and understand the process. It uses a combination of shell scripting cmds to complete the following stages and steps:

## 1. Refresh Util Image
By default we use the ts-utils, as declared in the Jenkinsfile under the variable: ***$BUILD_IMAGE***
- Remove any currently running ***$BUILD_IMAGE***
- Pulls the latest copy the ***$BUILD_IMAGE***
- Runs the pulled ***$BUILD_IMAGE***

## 2. WCB: Prepping the Environment
Using a series of docker commands
- It copies the git directory into the container ***$BUILD_IMAGE***, directory `/opt/WebSphere/CommerceServer90/src/`
- It then moves the `build.sh` and other specific assets into the container ***$BUILD_IMAGE***, directory`/opt/WebSphere/CommerceServer90/wcbd`


## 3. WCB: Compiling the BuildBundle
This is the stage where it calls wcb

## 4. Copying *BuildBundle* outside of ts-utils
This is the stage where it will copy the generated files outside of the ***$BUILD_IMAGE*** and will place it into the JENKINS_WORKSPACE of this job. For it to be used later.
