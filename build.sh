#!/bin/bash

set -eux

cd ../wcbd

export BUILD_TYPE="${2}"
export BUILD_NUMBER="${3}"
export DROP_LOC="${4}"
export CC="${1}-${BUILD_TYPE}" # CC is the customer code
export BUILD_LABEL=$CC-$BUILD_NUMBER

echo "############## Starting Build for $BUILD_LABEL of type $BUILD_TYPE from the following commit #########"

echo "############################## Directory Renaming #############################"
# Do any adjustments of the git repo for WCBD comsumption
# sed -i s/@@BUILD_NUMBER@@/$BUILD_LABEL/g ../src/workspace/DataLoad/sql/common/appconfigparams.sql || true
if [ "${BUILD_TYPE}" = "crs" ] ; then
	#cp -r ../src/crs/workspace/crs-web-delta/{src,WebContent} ../src/crs/workspace/crs-web || true
	#mv ../src/crs/workspace/crs-web/src/main/resources/* ../src/crs/workspace/crs-web/src/ || true
	#mv ../src/crs/workspace/crs-web-delta ../src/crs/workspace/crs-web-delta-old || true
  echo ""
fi


echo "############################## Starting Build #################################"
mkdir -p dist/server
bash wcbd-ant -file wcbd-build.xml -Dapp.type=${BUILD_TYPE} -Dbuild.label=$BUILD_LABEL -Dwork.dir .

# Move File to location
mv ./dist/server/*.zip ${DROP_LOC}
