#!/bin/sh

#set -x #same as set -o xtrace
set -e #same as set -o errexit
set -u #same as set -o nounset

##########################################################################################
# docker-build wrapper for docker-build with WC V9
#
#
##########################################################################################
# Versions
#

umask 0027

SCRIPT=$(readlink -f "$0")
SCRIPTDIR=`dirname "$SCRIPT"`
SCRIPTNAME=`basename $0`

set +u
if [ ! "$1" ] || [ ! "$2" ] ; then
    echo
    echo "You are missing one or more input variables."
    echo "Usage: docker-build.sh <app.type> <build.label>"
    echo
    exit 1
fi
set -u

##########################################################################################
# Main part of the script
##########################################################################################
cd ${SCRIPTDIR}

# SHOULD go into a properties file??
# SFTP_SERVER="dropbox.commerceservicesdevops.ibmcloud.com"
# SFTP_USER="dem"
# SFTP_KEY="~/.ssh/demo-sshkey"
# SFTP_PORT="20221"
# SFTP_DIR="~/wcbd"
DOCKER_REG="us.gcr.io/commerce-product/release/9.0.0"
# DOCKER_USER="hcl"
# DOCKER_PASSWORD="somePassword"
WC9_VERSION="9.1.5"
# DOCKER_NAMESPACE="custom"

# Putting Vars into a source file
#source docker-build.env

# Sample Build Label
#BUILD_TYPE="ts-app"
#BUILD_LABEL="demo-ts-21"
BUILD_TYPE=$1
BUILD_NUMBER=$2
IMAGE_TYPE='xc-app'
CC=$4

#Set build label
BUILD_LABEL=${BUILD_NUMBER}

##########################################################################################
# Step 0 - CD to build-type dir
#
##########################################################################################
cd ${BUILD_TYPE}

## Test if Docker Build exist
set +e
docker pull "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:${BUILD_LABEL}" > /dev/null 2>&1
ERROR_CODE=$?
set -e
if [ "$ERROR_CODE" -ne "0" ]; then

	echo "========================================================="
	echo " Building Docker for ${BUILD_TYPE}:${BUILD_LABEL}"
	echo "========================================================="
	echo

	##########################################################################################
	# Step 1 - Unzip file
	#
	##########################################################################################
	rm -fr CusDeploy
	ls -lrt
	echo ${BUILD_LABEL}
	mkdir CusDeploy
	unzip -q -d CusDeploy *${BUILD_LABEL}.zip


	##########################################################################################
	# Step 2 - Docker Build
	#
	##########################################################################################
	docker build -t "${DOCKER_REG}/${IMAGE_TYPE}:${BUILD_LABEL}" --build-arg IMAGE_REPO="${DOCKER_REG}" --build-arg IMAGE_TAG="${WC9_VERSION}" .
	echo "Pushing ${DOCKER_REG}/${IMAGE_TYPE}:${BUILD_LABEL}"
	docker push "${DOCKER_REG}/${IMAGE_TYPE}:${BUILD_LABEL}" > /dev/null 2>&1

	##########################################################################################
	# Step 3 - Clean Up
	#
	##########################################################################################
  rm -rf CusDeploy
	rm -fr *${BUILD_LABEL}.zip
	rm Dockerfile
	# docker rmi "${DOCKER_REG}/${DOCKER_NAMESPACE}/${IMAGE_TYPE}:${BUILD_LABEL}" > /dev/null 2>&1
	# docker rmi "${DOCKER_REG}/${DOCKER_NAMESPACE}/${IMAGE_TYPE}:latest" > /dev/null 2>&1
	# docker rmi "${DOCKER_REG}/commerce/${IMAGE_TYPE}:${WC9_VERSION}" > /dev/null 2>&1
else
	# docker rmi "${DOCKER_REG}/${DOCKER_NAMESPACE}/${IMAGE_TYPE}:${BUILD_LABEL}" > /dev/null 2>&1
	echo "========================================================="
	echo " Image already exists ${DOCKER_REG}/${DOCKER_NAMESPACE}/${IMAGE_TYPE}:${BUILD_LABEL}"
	echo "========================================================="
echo
fi
